#! /bin/bash

## Java 8 Oracle
# apt-get update
# apt-get install -y software-properties-common debconf-utils
# add-apt-repository -y ppa:webupd8team/java
# apt-get update
# echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | debconf-set-selections
# apt-get install -y oracle-java8-installer

## Java 8 OpenJDK
apt-get update
apt-get install -y openjdk-8-jdk

## Maven
apt-get install -y maven

## Util
apt-get install -y htop
apt-get install -y ack-grep
apt-get install -y unzip
apt-get install -y bc
apt-get install -y dos2unix
updatedb