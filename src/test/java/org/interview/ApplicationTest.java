package org.interview;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.interview.config.ApplicationConfiguration;
import org.interview.domain.Entries;
import org.interview.domain.Entry;
import org.interview.util.Utils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private ApplicationConfiguration config;

    @Autowired
    private ObjectMapper mapper;

    @BeforeClass
    public static void setupBefore() {
    }

    @AfterClass
    public static void setupAfter() {
    }

    @Autowired
    private MockMvc mvc;


    @Test
    public void testCreateGetDeleteEntry() throws Exception {
        String data = Utils.getLocalData(config.getDataFileId());
        Entries entries = readEntries(data);
        List<Entry> entryList = entries.getEntries();

        Random r = new Random();
        Integer i = r.nextInt(entryList.size());
        Entry entryFile = entryList.get(i);
        String entryJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(entryFile);

        mvc.perform(MockMvcRequestBuilders
            .post("/entry")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(entryJson))
            .andExpect(status().isCreated());

        String res = mvc.perform(MockMvcRequestBuilders
            .get(String.format("/entry/%s", entryFile.getId()))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

        Entry entryAPI = mapper.readValue(res, Entry.class);

        Assert.assertEquals(entryFile.getId(), entryAPI.getId());
        Assert.assertEquals(entryFile.getOrganisationId(), entryAPI.getOrganisationId());

        mvc.perform(MockMvcRequestBuilders
            .delete(String.format("/entry/%s", entryFile.getId()))
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }

    @Test
    public void testCreateGetDeleteAllEntries() throws Exception {
        String data = Utils.getLocalData(config.getDataFileId());
        Entries entries = readEntries(data);
        List<Entry> entryList = entries.getEntries();

        for (Entry entry : entryList) {
            String entryJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(entry);
            mvc.perform(MockMvcRequestBuilders
                .post("/entry")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(entryJson))
                .andExpect(status().isCreated());

            String res = mvc.perform(MockMvcRequestBuilders
                .get(String.format("/entry/%s", entry.getId()))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

            Entry entryAPI = mapper.readValue(res, Entry.class);

            Assert.assertEquals(entry.getId(), entryAPI.getId());
            Assert.assertEquals(entry.getOrganisationId(), entryAPI.getOrganisationId());
        }

        String dataRes = mvc.perform(MockMvcRequestBuilders
            .get("/entry")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

        Entries entriesRes = readEntries(dataRes);
        List<Entry> entryListRes = entriesRes.getEntries();

        for (Entry entry : entryListRes) {
            mvc.perform(MockMvcRequestBuilders
                .delete(String.format("/entry/%s", entry.getId()))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        }
    }

    private Entries readEntries(String s) {
        try {
            return mapper.readValue(s, Entries.class);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}