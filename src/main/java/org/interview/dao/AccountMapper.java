package org.interview.dao;

import org.interview.domain.Account;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;


public class AccountMapper implements RowMapper<Account> {
    public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Account(
            Optional.ofNullable(rs.getString("account_name")),
            rs.getString("account_number"),
            Optional.ofNullable(rs.getString("account_number_code")),
            Optional.ofNullable(rs.getInt("account_type")),
            Optional.ofNullable(rs.getString("address")),
            rs.getString("bank_id"),
            rs.getString("bank_id_code"),
            Optional.ofNullable(rs.getString("name")));
    }
}