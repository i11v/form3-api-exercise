package org.interview.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.interview.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;


@Repository
public class EntryDAO {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private String INSERT_ENTRY_SQL =
        "INSERT INTO entry VALUES(?,?,?,?)";

    private String INSERT_ENTRY_ATTRIBUTES_SQL =
        "INSERT INTO entry_attributes VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

    private String INSERT_ACCOUNT_SQL =
        "INSERT INTO account VALUES(?,?,?,?,?,?,?,?)";

    private String INSERT_FX_SQL =
        "INSERT INTO fx VALUES(?,?,?,?,?)";

    private String INSERT_CHARGE_INFORMATION_SQL =
        "INSERT INTO charge_information VALUES(?,?)";

    private String INSERT_CHARGE_SQL =
        "INSERT INTO charge (entry_id, type, amount, currency) VALUES(?,?,?,?)";

    private String SELECT_ENTRY_SQL =
        "SELECT * FROM entry t1 " +
        "JOIN entry_attributes t2 ON t1.id = t2.entry_id " +
        "JOIN fx t3 ON t1.id = t3.entry_id " +
        "JOIN charge_information t4 ON t1.id = t4.entry_id " +
        "WHERE t1.id = ?";

    private String SELECT_ALL_ENTRIES_SQL =
        "SELECT * FROM entry t1 " +
        "JOIN entry_attributes t2 ON t1.id = t2.entry_id " +
        "JOIN fx t3 ON t1.id = t3.entry_id " +
        "JOIN charge_information t4 ON t1.id = t4.entry_id";

    private String SELECT_ACCOUNT_SQL =
        "SELECT * FROM account WHERE account_number = ?";

    private String SELECT_CHARGE_SQL =
        "SELECT * FROM charge t1 " +
        "WHERE t1.entry_id = ? AND t1.type = ?";

    private String SELECT_ENTRY_EXISTS_SQL =
        "SELECT COUNT(*) FROM entry WHERE id = ?";

    private String DELETE_ENTRY_SQL =
        "DELETE FROM entry WHERE id = ?";


    // TODO Add Optinal
    public Entry findById(String id) {
        return jdbcTemplate.query(SELECT_ENTRY_SQL, new Object[]{id}, new ResultSetExtractor<Entry>() {
            public Entry extractData(ResultSet rs) throws SQLException {
                if (rs.next()) {
                    return buildEntry(rs);
                } else {
                    throw new SQLException(String.format("No Entry records found for id %s", id));
                }
            }
        });
    }

    public List<Entry> findAll() {
        return jdbcTemplate.query(SELECT_ALL_ENTRIES_SQL, new ResultSetExtractor<List<Entry>>() {
            public List<Entry> extractData(ResultSet rs) throws SQLException {
                List entries = new ArrayList();
                while (rs.next()) {
                    entries.add(buildEntry(rs));
                }
                return entries;
            }
        });
    }

    // TODO Put in transaction
    public void save(Entry e) {
        jdbcTemplate.update(INSERT_ENTRY_SQL,
            e.getId(),
            e.getType(),
            e.getVersion(),
            e.getOrganisationId());

        List<Account> accounts = Arrays.asList(e.getAttributes().getBeneficiaryParty(), e.getAttributes().getDebtorParty(),e.getAttributes().getSponsorParty());
        for (Account account : accounts) {
            try {
                jdbcTemplate.update(INSERT_ACCOUNT_SQL,
                    account.getAccountName().orElse(null),
                    account.getAccountNumber(),
                    account.getAccountNumberCode().orElse(null),
                    account.getAccountType().orElse(null),
                    account.getAddress().orElse(null),
                    account.getBankId(),
                    account.getBankIdCode(),
                    account.getName().orElse(null));
            } catch (DuplicateKeyException ex) {}
        }

        jdbcTemplate.update(INSERT_FX_SQL,
            e.getId(),
            e.getAttributes().getFx().getContractReference(),
            e.getAttributes().getFx().getExchangeRate(),
            e.getAttributes().getFx().getOriginalAmount(),
            e.getAttributes().getFx().getOriginalCurrency());

        jdbcTemplate.update(INSERT_CHARGE_INFORMATION_SQL,
            e.getId(),
            e.getAttributes().getChargesInformation().getBearerCode()
        );

        jdbcTemplate.update(INSERT_CHARGE_SQL,
            e.getId(),
            "receiver",
            e.getAttributes().getChargesInformation().getReceiverCharge().getAmount(),
            e.getAttributes().getChargesInformation().getReceiverCharge().getCurrency()
        );

        for (Charge charge : e.getAttributes().getChargesInformation().getSenderCharges()) {
            jdbcTemplate.update(INSERT_CHARGE_SQL,
                e.getId(),
                "sender",
                charge.getAmount(),
                charge.getCurrency()
            );
        }

        jdbcTemplate.update(INSERT_ENTRY_ATTRIBUTES_SQL,
            e.getId(),
            e.getAttributes().getAmount(),
            e.getAttributes().getBeneficiaryParty().getAccountNumber(),
            e.getAttributes().getCurrency(),
            e.getAttributes().getDebtorParty().getAccountNumber(),
            e.getAttributes().getEndToEndReference(),
            e.getAttributes().getNumericReference(),
            e.getAttributes().getPaymentId(),
            e.getAttributes().getPaymentPurpose(),
            e.getAttributes().getPaymentScheme(),
            e.getAttributes().getPaymentType(),
            e.getAttributes().getProcessingDate(),
            e.getAttributes().getReference(),
            e.getAttributes().getSchemePaymentSubType(),
            e.getAttributes().getSchemePaymentType(),
            e.getAttributes().getSponsorParty().getAccountNumber());
    }

    public Account findAccountByAccountNumber(String accountNumber) {
        Account account = jdbcTemplate.queryForObject(SELECT_ACCOUNT_SQL, new Object[]{accountNumber}, new AccountMapper());
        return account;
    }

    public List<Charge> findCharges(String id, String type) {
        return jdbcTemplate.query(SELECT_CHARGE_SQL, new Object[]{id, type}, new ResultSetExtractor<List<Charge>>() {
            public List<Charge> extractData(ResultSet rs) throws SQLException {
                List charges = new ArrayList();
                while (rs.next()) {
                    charges.add(buildCharge(rs));
                }
                return charges;
            }
        });
    }

    public Boolean entryExists(String id) {
        Integer c = jdbcTemplate.queryForObject(SELECT_ENTRY_EXISTS_SQL, new Object[]{id}, Integer.class);
        return c > 0;
    }

    public Boolean deleteById(String id){
        return jdbcTemplate.update(DELETE_ENTRY_SQL, new Object[]{id}) == 1;
    }

    private Charge buildCharge(ResultSet rs) throws SQLException {
        return new Charge(
            rs.getDouble("amount"),
            rs.getString("currency")
        );
    }

    private Entry buildEntry(ResultSet rs) throws SQLException {

        Charge receiverCharge = findCharges(rs.getString("id"), "receiver").get(0);
        List<Charge> senderCharges = findCharges(rs.getString("id"), "sender");

        return new Entry(
            rs.getString("id"),
            rs.getString("type"),
            rs.getInt("version"),
            rs.getString("organisation_id"),
            new EntryAttributes(
                rs.getDouble("amount"),
                findAccountByAccountNumber(rs.getString("beneficiary_party_account_number")),
                new ChargesInformation(
                    rs.getString("bearer_code"),
                    senderCharges,
                    receiverCharge
                ),
                rs.getString("currency"),
                findAccountByAccountNumber(rs.getString("debtor_party_account_number")),
                rs.getString("end_to_end_reference"),
                new FX(
                    rs.getString("contract_reference"),
                    rs.getDouble("exchange_rate"),
                    rs.getDouble("original_amount"),
                    rs.getString("original_currency")
                ),
                rs.getInt("numeric_reference"),
                rs.getString("payment_id"),
                rs.getString("payment_purpose"),
                rs.getString("payment_scheme"),
                rs.getString("payment_type"),
                rs.getString("processing_date"),
                rs.getString("reference"),
                rs.getString("scheme_payment_sub_type"),
                rs.getString("scheme_payment_type"),
                findAccountByAccountNumber(rs.getString("sponsor_party_account_number"))
            ));
    }
}