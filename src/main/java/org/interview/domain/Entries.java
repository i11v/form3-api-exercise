package org.interview.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


public class Entries {

    @Valid @NotNull @JsonProperty("data")
    private List<Entry> entries;

    public List<Entry> getEntries() {
        return entries;
    }
}
