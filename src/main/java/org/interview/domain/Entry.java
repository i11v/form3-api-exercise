package org.interview.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


public class Entry {

    @Valid @NotNull
    private String id;

    @Valid @NotNull
    private String type;

    @Valid @NotNull
    private Integer version;

    @Valid @NotNull
    private String organisationId;

    @Valid @NotNull
    private EntryAttributes attributes;

    public Entry(
        String id,
        String type,
        Integer version,
        String organisationId,
        EntryAttributes attributes) {
        this.id = id;
        this.type = type;
        this.version = version;
        this.organisationId = organisationId;
        this.attributes = attributes;
    }

    public String getId() {
        return id;
    }
    public String getType() {
        return type;
    }
    public Integer getVersion() {
        return version;
    }
    public String getOrganisationId() {
        return organisationId;
    }
    public EntryAttributes getAttributes() {
        return attributes;
    }
}
