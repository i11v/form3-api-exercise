package org.interview.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


public class Charge { ;

    @Valid @NotNull
    private Double chargeAmount;

    @Valid @NotNull
    private String chargeCurrency;

    public Charge(
        Double amount,
        String currency) {
        this.chargeAmount = amount;
        this.chargeCurrency = currency;
    }
    public Double getAmount() {
        return chargeAmount;
    }
    public String getCurrency() {
        return chargeCurrency;
    }
}
