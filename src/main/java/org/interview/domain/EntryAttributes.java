package org.interview.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


public class EntryAttributes {

    @Valid @NotNull
    private Double amount;

    @Valid @NotNull
    private Account beneficiaryParty;

    @Valid @NotNull
    private ChargesInformation chargesInformation;

    @Valid @NotNull
    private String currency;

    @Valid @NotNull
    private Account debtorParty;

    @Valid @NotNull
    private String endToEndReference;

    @Valid @NotNull
    private FX fx;

    @Valid @NotNull
    private Integer numericReference;

    @Valid @NotNull
    private String paymentId;

    @Valid @NotNull
    private String paymentPurpose;

    @Valid @NotNull
    private String paymentScheme;

    @Valid @NotNull
    private String paymentType;

    @Valid @NotNull
    private String processingDate;

    @Valid @NotNull
    private String reference;

    @Valid @NotNull
    private String schemePaymentSubType;

    @Valid @NotNull
    private String schemePaymentType;

    @Valid @NotNull
    private Account sponsorParty;

    public EntryAttributes(
        Double amount,
        Account beneficiaryParty,
        ChargesInformation chargesInformation,
        String currency,
        Account debtorParty,
        String endToEndReference,
        FX fx,
        Integer numericReference,
        String paymentId,
        String paymentPurpose,
        String paymentScheme,
        String paymentType,
        String processingDate,
        String reference,
        String schemePaymentSubType,
        String schemePaymentType,
        Account sponsorParty
    ) {
        this.amount = amount;
        this.beneficiaryParty = beneficiaryParty;
        this.chargesInformation = chargesInformation;
        this.currency = currency;
        this.debtorParty = debtorParty;
        this.endToEndReference = endToEndReference;
        this.fx = fx;
        this.numericReference = numericReference;
        this.paymentId = paymentId;
        this.paymentPurpose = paymentPurpose;
        this.paymentScheme = paymentScheme;
        this.paymentType = paymentType;
        this.processingDate = processingDate;
        this.reference = reference;
        this.schemePaymentSubType = schemePaymentSubType;
        this.schemePaymentType = schemePaymentType;
        this.sponsorParty = sponsorParty;
    }

    public Double getAmount() {
        return amount;
    }
    public Account getBeneficiaryParty() {
        return beneficiaryParty;
    }
    public ChargesInformation getChargesInformation() {
        return chargesInformation;
    }
    public String getCurrency() {
        return currency;
    }
    public Account getDebtorParty() {
        return debtorParty;
    }
    public String getEndToEndReference() {
        return endToEndReference;
    }
    public FX getFx() {
        return fx;
    }
    public Integer getNumericReference() {
        return numericReference;
    }
    public String getPaymentId() {
        return paymentId;
    }
    public String getPaymentPurpose() {
        return paymentPurpose;
    }
    public String getPaymentScheme() {
        return paymentScheme;
    }
    public String getPaymentType() {
        return paymentType;
    }
    public String getProcessingDate() {
        return processingDate;
    }
    public String getReference() {
        return reference;
    }
    public String getSchemePaymentSubType() {
        return schemePaymentSubType;
    }
    public String getSchemePaymentType() {
        return schemePaymentType;
    }
    public Account getSponsorParty() {
        return sponsorParty;
    }
}
