package org.interview.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Optional;


public class Account {

    @Valid
    private Optional<String> accountName;

    @Valid @NotNull
    private String accountNumber;

    @Valid
    private Optional<String> accountNumberCode;

    @Valid
    private Optional<Integer> accountType;

    @Valid
    private Optional<String> address;

    @Valid @NotNull
    private String bankId;

    @Valid @NotNull
    private String bankIdCode;

    @Valid
    private Optional<String> name;

    public Account(
        Optional<String> accountName,
        String accountNumber,
        Optional<String> accountNumberCode,
        Optional<Integer> accountType,
        Optional<String> address,
        String bankId,
        String bankIdCode,
        Optional<String> name) {
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.accountNumberCode = accountNumberCode;
        this.accountType = accountType;
        this.address = address;
        this.bankId = bankId;
        this.bankIdCode = bankIdCode;
        this.name = name;
    }

    public Optional<String> getAccountName() {
        return accountName;
    }
    public String getAccountNumber() {
        return accountNumber;
    }
    public Optional<String> getAccountNumberCode() {
        return accountNumberCode;
    }
    public Optional<Integer> getAccountType() {
        return accountType;
    }
    public Optional<String> getAddress() {
        return address;
    }
    public String getBankId() {
        return bankId;
    }
    public String getBankIdCode() {
        return bankIdCode;
    }
    public Optional<String> getName() {
        return name;
    }
}
