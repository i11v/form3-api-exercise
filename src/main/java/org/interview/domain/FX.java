package org.interview.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


public class FX {

    @Valid @NotNull
    private String contractReference;

    @Valid @NotNull
    private Double exchangeRate;

    @Valid @NotNull
    private Double originalAmount;

    @Valid @NotNull
    private String originalCurrency;

    public FX(
        String contractReference,
        Double exchangeRate,
        Double originalAmount,
        String originalCurrency) {
        this.contractReference = contractReference;
        this.exchangeRate = exchangeRate;
        this.originalAmount = originalAmount;
        this.originalCurrency = originalCurrency;
    }

    public String getContractReference() {
        return contractReference;
    }
    public Double getExchangeRate() {
        return exchangeRate;
    }
    public Double getOriginalAmount() {
        return originalAmount;
    }
    public String getOriginalCurrency() {
        return originalCurrency;
    }
}
