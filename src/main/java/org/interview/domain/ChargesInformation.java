package org.interview.domain;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;


public class ChargesInformation {

    @Valid @NotNull
    private String bearerCode;

    @Valid @NotNull
    private List<Charge> senderCharges;

    @Valid @NotNull
    private Charge receiverCharge;

    public ChargesInformation(
        String bearerCode,
        List<Charge> senderCharges,
        Charge receiverCharge
    ) {
        this.bearerCode = bearerCode;
        this.senderCharges = senderCharges;
        this.receiverCharge = receiverCharge;
    }

    public String getBearerCode() {
        return bearerCode;
    }
    public List<Charge> getSenderCharges() {
        return senderCharges;
    }
    public Charge getReceiverCharge() {
        return receiverCharge;
    }
}
