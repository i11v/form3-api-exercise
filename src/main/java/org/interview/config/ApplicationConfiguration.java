package org.interview.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class ApplicationConfiguration {

    private String dataFileId;

    public String getDataFileId() {
        return dataFileId;
    }
    public void setDataFileId(String dataFileId) {
        this.dataFileId = dataFileId;
    }
}