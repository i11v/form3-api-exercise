package org.interview.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    public static String getLocalData(String id) {
        URL resource = Utils.class.getResource(String.format("/%s.json", id));
        try {
            File f = Paths.get(resource.toURI()).toFile();
            return new String(Files.readAllBytes(f.toPath()));
        } catch (IOException | URISyntaxException e) {
            log.error(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
