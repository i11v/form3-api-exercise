package org.interview.util.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.interview.domain.Charge;
import org.interview.domain.ChargesInformation;

import java.io.IOException;

public class ChargesInformationSerializer extends StdSerializer<ChargesInformation> {

    public ChargesInformationSerializer() {
        this(null);
    }

    public ChargesInformationSerializer(Class<ChargesInformation> t) {
        super(t);
    }

    @Override
    public void serialize(ChargesInformation value, JsonGenerator jgen, SerializerProvider provider) throws IOException {

        jgen.writeStartObject();
        jgen.writeStringField("bearer_code", value.getBearerCode());
        jgen.writeArrayFieldStart("sender_charges");
        for (Charge charge : value.getSenderCharges()) {
            jgen.writeStartObject();
            jgen.writeNumberField("amount", charge.getAmount());
            jgen.writeStringField("currency", charge.getCurrency());
            jgen.writeEndObject();
        }
        jgen.writeEndArray();
        jgen.writeNumberField("receiver_charges_amount", value.getReceiverCharge().getAmount());
        jgen.writeStringField("receiver_charges_currency", value.getReceiverCharge().getCurrency());
        jgen.writeEndObject();
    }
}
