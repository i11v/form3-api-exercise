package org.interview.util.json;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.interview.domain.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class EntryDeserializer extends StdDeserializer<Entry> {

    private final Logger log = LoggerFactory.getLogger(getClass());

    public EntryDeserializer() {
        this(null);
    }

    public EntryDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public Entry deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);
        String id = node.get("id").asText();
        String type = node.get("type").asText();
        Integer version = node.get("version").asInt();
        String organisationId = node.get("organisation_id").asText();
        JsonNode attributes = node.get("attributes");
        Double amount = attributes.get("amount").asDouble();

        Map<String, Account> parties = Arrays.asList("beneficiary_party", "debtor_party", "sponsor_party")
            .stream()
            .collect(Collectors.toMap(p -> p, p -> {
                JsonNode partyNode = attributes.get(p);
                Optional<String> partyAccountName = Optional.ofNullable(partyNode)
                    .filter(n -> n.hasNonNull("account_name"))
                    .map(n -> n.get("account_name").asText());
                String partyAccountNumber = partyNode.get("account_number").asText();
                Optional<String> partyAccountNumberCode = Optional.ofNullable(partyNode)
                    .filter(n -> n.hasNonNull("account_number_code"))
                    .map(n -> n.get("account_number_code").asText());
                Optional<Integer> partyAccountType = Optional.ofNullable(partyNode)
                    .filter(n -> n.hasNonNull("account_type"))
                    .map(n -> n.get("account_type").asInt());
                Optional<String> partyAddress = Optional.ofNullable(partyNode)
                    .filter(n -> n.hasNonNull("address"))
                    .map(n -> n.get("address").asText());
                String partyBankId = partyNode.get("bank_id").asText();
                String partyBankIdCode = partyNode.get("bank_id_code").asText();
                Optional<String> partyName = Optional.ofNullable(partyNode)
                    .filter(n -> n.hasNonNull("name"))
                    .map(n -> n.get("name").asText());
                return new Account(
                    partyAccountName,
                    partyAccountNumber,
                    partyAccountNumberCode,
                    partyAccountType,
                    partyAddress,
                    partyBankId,
                    partyBankIdCode,
                    partyName);
            }));

        JsonNode chargesInformationNode = attributes.get("charges_information");
        String bearerCode = chargesInformationNode.get("bearer_code").asText();
        Double receiverChargesAmount = chargesInformationNode.get("receiver_charges_amount").asDouble();
        String receiverChargesCurrency = chargesInformationNode.get("receiver_charges_currency").asText();
        Charge receiverCharge = new Charge(receiverChargesAmount, receiverChargesCurrency);
        Iterator<JsonNode> senderChargesNode = chargesInformationNode.get("sender_charges").elements();
        Iterable<JsonNode> senderChargesNodeIterable = () -> senderChargesNode;
        List<Charge> senderCharges = StreamSupport
            .stream(senderChargesNodeIterable.spliterator(), false)
            .map(n -> new Charge(n.get("amount").asDouble(), n.get("currency").asText()))
            .collect(Collectors.toList());
        String currency = attributes.get("currency").asText();
        String endToEndReference = attributes.get("end_to_end_reference").asText();
        JsonNode fxNode = attributes.get("fx");
        String fxContractReference = fxNode.get("contract_reference").asText();
        Double fxExchangeRate = fxNode.get("exchange_rate").asDouble();
        Double fxOriginalAmount = fxNode.get("original_amount").asDouble();
        String fxOriginalCurrency = fxNode.get("original_currency").asText();
        Integer numericReference = attributes.get("numeric_reference").asInt();
        String paymentId = attributes.get("payment_id").asText();
        String paymentPurpose = attributes.get("payment_purpose").asText();
        String paymentScheme = attributes.get("payment_scheme").asText();
        String paymentType = attributes.get("payment_type").asText();
        String processingDate = attributes.get("processing_date").asText();
        String reference = attributes.get("reference").asText();
        String schemePaymentSubType = attributes.get("scheme_payment_sub_type").asText();
        String schemePaymentType = attributes.get("scheme_payment_type").asText();

        ChargesInformation chargesInformation = new ChargesInformation(bearerCode, senderCharges, receiverCharge); // senderCharges); //, receiverCharge);
        FX fx = new FX(fxContractReference, fxExchangeRate, fxOriginalAmount, fxOriginalCurrency);
        EntryAttributes paymentAttributes = new EntryAttributes(
            amount,
            parties.get("beneficiary_party"),
            chargesInformation,
            currency,
            parties.get("debtor_party"),
            endToEndReference,
            fx,
            numericReference,
            paymentId,
            paymentPurpose,
            paymentScheme,
            paymentType,
            processingDate,
            reference,
            schemePaymentSubType,
            schemePaymentType,
            parties.get("sponsor_party"));

        return new Entry(id, type, version, organisationId, paymentAttributes);
    }
}