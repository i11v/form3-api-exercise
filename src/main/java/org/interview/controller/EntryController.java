package org.interview.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.interview.dao.EntryDAO;
import org.interview.domain.Entry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.*;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


@RestController
public class EntryController {

    private static final Logger log = LoggerFactory.getLogger(EntryController.class);

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private EntryDAO entryDAO;

    @RequestMapping(value = "/entry", method = GET)
    public ResponseEntity<Map<String, List<Entry>>> listAllEntries() {
        List<Entry> entries = entryDAO.findAll();
        if (entries.isEmpty()){
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        Map<String, List<Entry>> res = new HashMap<>();
        res.put("data", entries);
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/entry/{id}", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Entry> getEntry(@PathVariable("id") String id) {
        log.info("Fetching Entry with Id " + id);
        Entry entry = entryDAO.findById(id);
        if (entry == null) {
            log.warn("Entry with Id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(entry, HttpStatus.OK);
    }

    @RequestMapping(value = "/entry", method = POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createEntry(@RequestBody Entry entry, UriComponentsBuilder ucBuilder) {
        log.info("Creating Entry with Id: " + entry.getId());

        if (entryDAO.entryExists(entry.getId())) {
            log.warn("An Entry with Id " + entry.getId() + " already exist");
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        entryDAO.save(entry);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/entry/{id}").buildAndExpand(entry.getId()).toUri());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/entry/{id}", method = DELETE)
    public ResponseEntity<Void> deleteEntry(@PathVariable("id") String id) {
        if (!entryDAO.entryExists(id)) {
            log.warn("Entry with Id " + id + " not found");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        if (entryDAO.deleteById(id)){
            log.info("Deleting Entry with Id " + id);
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
            log.warn("Could not delete Entry with Id " + id);
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
    }
}
