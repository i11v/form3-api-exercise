package org.interview;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Component
public class ApplicationRunner implements CommandLineRunner {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public void run(String... args) {
        log.info("Running App");
    }
}
