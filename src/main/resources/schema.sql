DROP TABLE IF EXISTS entry;
DROP TABLE IF EXISTS entry_attributes;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS charge_information;
DROP TABLE IF EXISTS charge;
DROP TABLE IF EXISTS fx;

CREATE TABLE entry (
    id VARCHAR(36) NOT NULL,
    type VARCHAR(256) NOT NULL,
    version INT NOT NULL,
    organisation_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE entry_attributes (
    entry_id VARCHAR(36),
    amount DECIMAL(13, 4) NOT NULL,
    beneficiary_party_account_number VARCHAR(36) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    debtor_party_account_number VARCHAR(36) NOT NULL,
    end_to_end_reference VARCHAR(256) NOT NULL,
    numeric_reference INT NOT NULL,
    payment_id VARCHAR(256) NOT NULL,
    payment_purpose VARCHAR(1000) NOT NULL,
    payment_scheme VARCHAR(256) NOT NULL,
    payment_type VARCHAR(256) NOT NULL,
    processing_date VARCHAR(256) NOT NULL,
    reference VARCHAR(256) NOT NULL,
    scheme_payment_sub_type VARCHAR(256) NOT NULL,
    scheme_payment_type VARCHAR(256) NOT NULL,
    sponsor_party_account_number VARCHAR(36) NOT NULL,
    PRIMARY KEY (entry_id),
    CONSTRAINT fk_entry_attributes_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);

CREATE TABLE account (
    account_name VARCHAR(256),
    account_number VARCHAR(256) NOT NULL,
    account_number_code VARCHAR(256),
    account_type VARCHAR(256),
    address VARCHAR(1000),
    bank_id INT NOT NULL,
    bank_id_code VARCHAR(256) NOT NULL,
    name VARCHAR(1000),
    PRIMARY KEY (account_number)
);

CREATE TABLE charge_information (
    entry_id VARCHAR(36),
    bearer_code VARCHAR(256) NOT NULL,
    PRIMARY KEY (entry_id),
    CONSTRAINT fk_charge_information_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);

CREATE TABLE charge (
    id INT AUTO_INCREMENT,
    entry_id VARCHAR(36) NOT NULL,
    type VARCHAR(256) NOT NULL,
    amount DECIMAL(13, 4) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_charge_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);

CREATE TABLE fx (
    entry_id VARCHAR(36),
    contract_reference VARCHAR(256),
    exchange_rate DECIMAL(13, 4),
    original_amount DECIMAL(13, 4),
    original_currency VARCHAR(3),
    PRIMARY KEY (entry_id),
    CONSTRAINT fk_fx_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);
