# Form3 API Exercise

## Running (without provided VM)

### Prerequisites

You need to install:

* [JDK8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) - Java SE Development Kit 8 
* [Maven](https://maven.apache.org/download.cgi) - Build tool

Clone the repository. cd into the root dir of the repo and run: 

Compile source with:

`mvn compile`

Run tests with:

`mvn test`

Run server with:

`mvn spring-boot:run`

## Running On VM 

### Prerequisites

You need to install:

* [Virtualbox](https://www.virtualbox.org/) - Virtualization 
* [Vagrant](https://www.vagrantup.com/) - Development Environment


### VM Usage

Clone the repository. CD into the root dir of the repo and run: 

```
vagrant up
```

After the VM is up and provisioned SSH into it with:

```
vagrant ssh
```

The repo directory is synced on your host and guest so any changes will be automatically applied.

The VM comes provisioned with Ubuntu and the following packages:

* java8
* maven
* git, ack-grep etc. util libs

To recreate the VM you can do:
```
vagrant destroy
vagrant up
```
To reload the VM (when you add new VM settings like ports etc.)
```
vagrant reload
```

For running the source see above section for Maven commands

## Architecture/Design

The service uses Spring Boot, SQL Database (H2 by default but can be swapped with any database)
JDBC for communication with the DB (although Hibernate reduces boilerplate it's very inflexible 
when developing non trivial applications), Jackson as JSON object mapper and a REST HTTP interface.

The domain is split into the following models: Entry, EntryAttributes, Account, 
ChargesInformation, Charge and FX. The entry model maps exactly to the provided 
sample JSON (using some custom serialization). 

Below is the DB schema representation of the models. 

```sql

CREATE TABLE entry (
    id VARCHAR(36) NOT NULL,
    type VARCHAR(256) NOT NULL,
    version INT NOT NULL,
    organisation_id VARCHAR(36) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE entry_attributes (
    entry_id VARCHAR(36),
    amount DECIMAL(13, 4) NOT NULL,
    beneficiary_party_account_number VARCHAR(36) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    debtor_party_account_number VARCHAR(36) NOT NULL,
    end_to_end_reference VARCHAR(256) NOT NULL,
    numeric_reference INT NOT NULL,
    payment_id VARCHAR(256) NOT NULL,
    payment_purpose VARCHAR(1000) NOT NULL,
    payment_scheme VARCHAR(256) NOT NULL,
    payment_type VARCHAR(256) NOT NULL,
    processing_date VARCHAR(256) NOT NULL,
    reference VARCHAR(256) NOT NULL,
    scheme_payment_sub_type VARCHAR(256) NOT NULL,
    scheme_payment_type VARCHAR(256) NOT NULL,
    sponsor_party_account_number VARCHAR(36) NOT NULL,
    PRIMARY KEY (entry_id),
    CONSTRAINT fk_entry_attributes_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);

CREATE TABLE account (
    account_name VARCHAR(256),
    account_number VARCHAR(256) NOT NULL,
    account_number_code VARCHAR(256),
    account_type VARCHAR(256),
    address VARCHAR(1000),
    bank_id INT NOT NULL,
    bank_id_code VARCHAR(256) NOT NULL,
    name VARCHAR(1000),
    PRIMARY KEY (account_number)
);

CREATE TABLE charge_information (
    entry_id VARCHAR(36),
    bearer_code VARCHAR(256) NOT NULL,
    PRIMARY KEY (entry_id),
    CONSTRAINT fk_charge_information_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);

CREATE TABLE charge (
    id INT AUTO_INCREMENT,
    entry_id VARCHAR(36) NOT NULL,
    type VARCHAR(256) NOT NULL,
    amount DECIMAL(13, 4) NOT NULL,
    currency VARCHAR(3) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_charge_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);

CREATE TABLE fx (
    entry_id VARCHAR(36),
    contract_reference VARCHAR(256),
    exchange_rate DECIMAL(13, 4),
    original_amount DECIMAL(13, 4),
    original_currency VARCHAR(3),
    PRIMARY KEY (entry_id),
    CONSTRAINT fk_fx_entry_id FOREIGN KEY (entry_id) REFERENCES entry (id) ON DELETE CASCADE
);

```

## REST API
 
### List of REST API endpoints:
 
Locally the application is running on localhost:8080
 
#### Get All Entries: 
 
* Method: GET
* Example URL: `/entry`
* Sample Response (JSON): List of the below provided JSON object enclosed in "data" key
 
 ```json
 {
  "data": [{entryJson},...]
 }
 ```
 
#### Get Entry by Id: 
 
* Method: GET
* Example URL: `/entry/{id}`
* Sample Response (JSON):

```json
{
  "id" : "7f172f5c-f810-4ebe-b015-cb1fc24c6b66",
  "type" : "Payment",
  "version" : 0,
  "organisation_id" : "743d5b63-8e6f-432e-a8fa-c5d8d2ee5fcb",
  "attributes" : {
    "amount" : 100.21,
    "beneficiary_party" : {
      "account_name" : "W Owens",
      "account_number" : "31926819",
      "account_number_code" : "BBAN",
      "account_type" : 0,
      "address" : "1 The Beneficiary Localtown SE2",
      "bank_id" : "403000",
      "bank_id_code" : "GBDSC",
      "name" : "Wilfred Jeremiah Owens"
    },
    "charges_information" : {
      "bearer_code" : "SHAR",
      "sender_charges" : [ {
        "amount" : 5.0,
        "currency" : "GBP"
      }, {
        "amount" : 10.0,
        "currency" : "USD"
      } ],
      "receiver_charges_amount" : 1.0,
      "receiver_charges_currency" : "USD"
    },
    "currency" : "GBP",
    "debtor_party" : {
      "account_name" : "EJ Brown Black",
      "account_number" : "GB29XABC10161234567801",
      "account_number_code" : "IBAN",
      "address" : "10 Debtor Crescent Sourcetown NE1",
      "bank_id" : "203301",
      "bank_id_code" : "GBDSC",
      "name" : "Emelia Jane Brown"
    },
    "end_to_end_reference" : "Wil piano Jan",
    "fx" : {
      "contract_reference" : "FX123",
      "exchange_rate" : 2.0,
      "original_amount" : 200.42,
      "original_currency" : "USD"
    },
    "numeric_reference" : 1002001,
    "payment_id" : "123456789012345678",
    "payment_purpose" : "Paying for goods/services",
    "payment_scheme" : "FPS",
    "payment_type" : "Credit",
    "processing_date" : "2017-01-18",
    "reference" : "Payment for Em's piano lessons",
    "scheme_payment_sub_type" : "InternetBanking",
    "scheme_payment_type" : "ImmediatePayment",
    "sponsor_party" : {
      "account_number" : "56781234",
      "bank_id" : "123123",
      "bank_id_code" : "GBDSC"
    }
  }
}
```

#### Create Entry with the above JSON body in the POST request: 
 
* Method: POST
* Example URL: `/entry`

#### Delete Entry  
 
* Method: DELETE
* Example URL: `/entry/{id}`


## TODO (improvements)

* Add update endpoint (although I strongly prefer append only, no in place modification)
* Add transactions for inserts
* Add version to API
* Add Datetime field type for processing_date + in DB Date type 
* Add created timestamp fields to DB/models 
* Add metrics (Graphite/Grafana)
* Add Docker container service
* Add configs for different DB backends MySQL/Postgres
* Add property based testing/data generators based on models 